package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.cache.CacheKey;
import io.quarkus.panache.common.Sort;
import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.keys.KeyCommands;
import io.quarkus.redis.datasource.value.ValueCommands;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import model.Student;
import org.bson.types.ObjectId;
import repository.StudentRepository;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class StudentService {
    private static final String STUDENT_CACHE_GET ="student: ";
    private final KeyCommands<String> keyCommands;
    private final ValueCommands<String, String> valueCommands;
    @Inject
    StudentRepository studentRepository;

     public StudentService(RedisDataSource ds){
         keyCommands = ds.key();
         valueCommands=ds.value(String.class,String.class);
     }

     public List<Student> getListStudents(){
         return studentRepository.listAll(Sort.by("firstName"));
     }

    public List<Student> listStudentByCourse(String course){
         return studentRepository.getStudentByCourse(course);
    }

     public Student getStudent(@CacheKey String id){

         String cacheStudent = valueCommands.get(STUDENT_CACHE_GET + id);
         if(cacheStudent!=null){
             return deserialization(cacheStudent);
         }

         Student student = studentRepository.findById(new ObjectId(id));
         if (student != null){
             valueCommands.set(STUDENT_CACHE_GET + id,serialization(student));
         }
         return student;
     }

     public void createStudent(Student student){
         studentRepository.persist(student);
         valueCommands.set(STUDENT_CACHE_GET+student,serialization(student));
     }

     public Boolean updateStudent(String id, Student student){
         Optional<Student> st = studentRepository.findByIdOptional(new ObjectId(id));
         if (st.isPresent()){
             student.setId(new ObjectId(id).toString());
             studentRepository.update(student);
             valueCommands.set(STUDENT_CACHE_GET+id,serialization(student));
             return true;
         }else
             return false;
     }

     public Boolean deleteStudent(String id){
         Optional<Student> student = studentRepository.findByIdOptional(new ObjectId(id));
         if (student.isPresent()){
             studentRepository.deleteById(new ObjectId(id));
             keyCommands.del(STUDENT_CACHE_GET+id);
             return true;
         }else {
             return false;
         }
     }

    private String serialization(Student student) {
         try{
             ObjectMapper objectMapper = new ObjectMapper();
             return objectMapper.writeValueAsString(student);
         }catch (JsonProcessingException e){
             throw new RuntimeException("Error di serialization student", e);
         }
    }

    private Student deserialization(String cacheStudent) {
         try{
             ObjectMapper objectMapper = new ObjectMapper();
             return objectMapper.readValue(cacheStudent, Student.class);
         }catch (JsonProcessingException e){
             throw new RuntimeException("Error di deserialization dell'oggetto Student", e);
         }

    }
}
