package controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import model.Student;
import service.StudentService;

import java.util.List;

@Path("/students")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ResourceStudente {

    @Inject
    StudentService studentService;

    @GET
    public List<Student> getStudents(){
        return studentService.getListStudents();
    }

    @GET
    @Path("/{id}")
    public Response getStudent(@PathParam("id") String id){
         Student student=studentService.getStudent(id);
         return Response.ok(student).build();
    }

    @POST
    public Response createStudent(Student student){
        studentService.createStudent(student);
        return Response.status(Response.Status.CREATED).build();
    }
    @PUT
    @Path("/update/{id}")
    public Response updateStudent(@PathParam("id") String id,Student student){
        if(studentService.updateStudent(id,student)){
            return Response.ok().build();
        }else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    @DELETE
    @Path("/{id}")
    public Response deleteStudent(@PathParam("id") String id){
        if(studentService.deleteStudent(id)){
            return Response.status(Response.Status.NO_CONTENT).build();
        }else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

    }
}
