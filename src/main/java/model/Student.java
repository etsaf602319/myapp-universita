package model;

import io.quarkus.mongodb.panache.common.MongoEntity;
import org.bson.BsonType;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.codecs.pojo.annotations.BsonRepresentation;


@MongoEntity(collection = "student")
public class Student {

    @BsonId
    @BsonRepresentation(BsonType.OBJECT_ID)
    public String id;
    @BsonProperty("firstname")
    public String firstName;

    @BsonProperty("lastname")
    public String lastName;

    @BsonProperty("age")
    public int age;

    @BsonProperty("course")
    public String course;

    public Student() {
    }

    public Student(String id, String firstName, String lastName, int age, String course) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.course = course;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
