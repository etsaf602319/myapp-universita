package repository;

import io.quarkus.mongodb.panache.PanacheMongoRepository;
import jakarta.enterprise.context.ApplicationScoped;
import model.Student;

import java.util.List;

@ApplicationScoped
public class StudentRepository implements PanacheMongoRepository<Student> {

    public List<Student> getStudentByCourse(String course){
        return list("course",course);
    }
}
