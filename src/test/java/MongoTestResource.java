import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import org.testcontainers.containers.MongoDBContainer;

import java.util.HashMap;
import java.util.Map;

public class MongoTestResource implements QuarkusTestResourceLifecycleManager {

    private static final MongoDBContainer MONGO = new MongoDBContainer("mongo:4.0.10");

    public Map<String, String> start() {
        MONGO.start();
        Map<String, String> properties = new HashMap<>();
        properties.put("quarkus.mongodb.connection-string", MONGO.getReplicaSetUrl());
        return properties;
    }

    @Override
    public void stop() {
       MONGO.stop();
    }
}
