import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import jakarta.inject.Inject;
import model.Student;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.junit.jupiter.Testcontainers;
import repository.StudentRepository;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@Testcontainers
@QuarkusTestResource(MongoTestResource.class)
public class ResourceStudenteTest {

    @Inject
    StudentRepository studentRepository;
    @BeforeEach
    public void setUp() {
        RestAssured.baseURI = "http://localhost:8083";  // Assicurati che la porta sia quella corretta
    }

    @Test
    public void testGetStudents() {
        // Crea un documento di esempio
        Student student = new Student();
        student.firstName = "Valente";
        student.lastName = "Anna";
        student.age = 23;
        studentRepository.persist(student);

        given()
                .when().get("/students")
                .then()
                .statusCode(200)
                .body("$.size()", is(4), "[0].firstName", is("Valente"),
                        "[0].lastName",is("Anna"),
                        "[0].age", is(23));
    }

    @Test
    public void testGetStudent() {
        // Crea un documento di esempio
        Student student = new Student();
        student.firstName = "Valente";
        student.lastName = "Anna";
        student.age = 23;
        studentRepository.persist(student);

        given()
                .when().get("/students/" + student.id)
                .then()
                .statusCode(200)
                .body("firstName", is("Valente"));
    }

    @Test
    public void testCreateStudent() {
        Student student = new Student();
        student.firstName = "Valente";
        student.lastName = "Anna";
        student.age = 23;

        given()
                .body(student)
                .header("Content-Type", "application/json")
                .when().post("/students")
                .then()
                .statusCode(201);

        // Verifica che lo studente sia stato creato
        assert(studentRepository.count() == 1);
    }

    @Test
    public void testUpdateStudentSuccess() {
        // Crea un documento di esempio
        Student student = new Student();
        student.firstName = "Valente";
        student.lastName = "Anna";
        student.age = 23;
        studentRepository.persist(student);

        Student updatedStudent = new Student();
        updatedStudent.firstName= "Jane";

        given()
                .body(updatedStudent)
                .header("Content-Type", "application/json")
                .when().put("/students/update/" + student.id)
                .then()
                .statusCode(200);

        // Verifica che lo studente sia stato aggiornato
        Student updatedEntity = studentRepository.findById(new ObjectId(student.id));
        assert(updatedEntity.firstName.equals("Jane"));
    }

    @Test
    public void testDeleteStudentSuccess() {
        // Crea un documento di esempio
        Student student = new Student();
        student.firstName = "Valente";
        student.lastName = "Anna";
        student.age = 23;
        studentRepository.persist(student);

        given()
                .when().delete("/students/" + student.id)
                .then()
                .statusCode(204);

        // Verifica che lo studente sia stato cancellato
        assert(student.firstName.equals("Valente"));
    }
}
